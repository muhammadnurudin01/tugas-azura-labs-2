"use strict";
let bcrypt = require("bcryptjs");
const { use } = require("passport");
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class user extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	user.init(
		{
			username: DataTypes.STRING,
			password: DataTypes.STRING,
		},
		{
			hooks: {
				beforeCreate: (user, opt) => {
					user.password = bcrypt.hashSync(user.password, 10);
				},
			},
			sequelize,
			modelName: "user",
		}
	);
	return user;
};
