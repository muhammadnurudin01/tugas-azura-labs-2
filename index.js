let express = require("express");
let bodyParser = require("body-parser");
let userModels = require("./models").user;
let bcrypt = require("bcryptjs");
let app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/register", (req, res) => {
	userModels
		.create(req.body)
		.then(function (data) {
			res.json({ data: data });
		})
		.catch(function (error) {
			res.json({ er: error.message });
		});
});

app.post("/login", (req, res) => {
	userModels
		.findOne({
			where: {
				username: req.body.username,
			},
		})
		.then(function (data) {
			if (data) {
				let hash = bcrypt.compareSync(req.body.password, data.password);
				if (hash) {
					res.send("Welcome");
				} else {
					res.send("Incorrect Password");
				}
			} else {
				res.send("Null");
			}
		})
		.catch(function (error) {
			res.json({ er: error });
		});
});
//assign the port
let port = 3200;
app.listen(port, () => console.log("server running at port " + port));
